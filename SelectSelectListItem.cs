IEnumerable<SelectListItem> listItems = Enum.GetValues(typeof(CategoryType)).Cast<CategoryType>().Select(v => new SelectListItem
            {
                Selected = (v == _categoryType),
                Text = v.ToString(),
                Value = ((int)v).ToString(),
            });
			